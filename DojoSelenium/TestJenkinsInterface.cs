﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DojoSelenium
{
    [TestFixture] 
    public class TestJenkinsInterface
    {
        private IWebDriver driver;

        [Test]
        public void AddAndDeleteNewEmptyProject()
        {
            driver = new ChromeDriver();

            driver.Navigate().GoToUrl("http://localhost:8094/");
            driver.FindElement(By.CssSelector("#tasks > div:nth-child(1) > a.task-link")).Click();

            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0,0,5));
            wait.Until(w => driver.FindElement(By.Id("name")).Displayed);

            driver.FindElement(By.Id("name")).SendKeys("EmptyTestProject");
            driver.FindElement(By.CssSelector("#j-add-item-type-standalone-projects > ul > li.hudson_model_FreeStyleProject")).Click();

            driver.FindElement(By.Id("ok-button")).Click();
            driver.FindElement(By.Id("yui-gen41-button")).Click();

            driver.Navigate().GoToUrl("http://localhost:8094/job/EmptyTestProject/");
            driver.FindElement(By.CssSelector("#tasks > div:nth-child(7) > a.task-link")).Click();

            WebDriverWait alertWait = new WebDriverWait(driver, new TimeSpan(3000));
            alertWait.Until(ExpectedConditions.AlertIsPresent());
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();

            driver.Navigate().GoToUrl("http://localhost:8094/job/Empty/");
            var screenMessageElement = driver.FindElement(By.CssSelector("body > h2"));
            var screenMessage = screenMessageElement.Text;

            Assert.AreEqual("HTTP ERROR 404", screenMessage);

            driver.Close();
            driver.Quit();
        }
    }
}
