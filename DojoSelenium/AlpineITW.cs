﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DojoSelenium
{
    [TestFixture] 
    public class AlpineITW
    {
        private IWebDriver driver;

        [Test]
        public void TestRegistrationPageCaptcha()
        {
            driver = new ChromeDriver();

            driver.Navigate().GoToUrl("https://alpineitw.com/sign-up-form/");
            driver.FindElement(By.Name("your-name")).SendKeys("Adam");
            driver.FindElement(By.Name("email-signup")).SendKeys("adam@alpineitw.com");
            driver.FindElement(By.Name("text-company")).SendKeys("Adam ITW");
            driver.FindElement(By.Name("tel-number")).SendKeys("57319");

            driver.FindElement(By.CssSelector("#wpcf7-f4-p3073-o1 > form > div:nth-child(10) > div.signupinput > input")).Click();

            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            wait.Until(w => driver.FindElement(By.CssSelector("#wpcf7-f4-p3073-o1 > form > div:nth-child(9) > div.signupinput > div > span")).Displayed);

            var screenErrorElement = driver.FindElement(By.CssSelector("#wpcf7-f4-p3073-o1 > form > div:nth-child(9) > div.signupinput > div > span"));
            var screenMessage = screenErrorElement.Text;

            Assert.AreEqual("Please verify that you are not a robot.", screenMessage);

            driver.Close();
            driver.Quit();
        }
    }
}
